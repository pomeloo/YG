> &#x1F332; 连享会：因果推断/内生性 专题

# 连享会：因果推断/内生性 专题

> &#x1F34E;  点击右上角的【**Fork**】按钮，可以把这个项目完整复制到你的码云账号下，随时查看。   

> &#x1F449;  点击上方 [【Wiki】](https://gitee.com/arlionn/YG/wikis) 查看常见问题解答 ( [**FAQs**](https://gitee.com/arlionn/YG/wikis/YG-Day1-WangCT.md?sort_id=3098277))。

&emsp;


## 1. 课程概览

> &#x1F449;  点击右上角的【**Fork**】按钮，可以把这个项目完整复制到你的码云账号下，随时查看。 

- **时间：** 2020 年 11 月 12-15 日 (周四-周日)    
  - 上午 9:00-12:00，下午 14:30-17:30 (17:30-18:00 答疑)。   
- **方式：** 线上直播  
- **授课嘉宾：** 王存同 (中央财经大学)；司继春 (上海对外经贸大学)  
- **报名链接：** <http://junquan18903405450.mikecrm.com/HFMDelY>   
- **课程主页：** <https://gitee.com/arlionn/YG>  
- **Note:** 预习资料、常见问题解答等都将通过该主页发布。
  - &#x1F353; [**下载**-课程中涉及的论文合集](https://www.jianguoyun.com/p/DUasG80QtKiFCBjHpskD)

&emsp;

## 2. 嘉宾简介

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/王存同生活照竖版200.png)

[王存同](http://ssp.cufe.edu.cn/info/1127/3289.htm)，中央财经大学教授。博士毕业于北京大学 (与 University of Michigan 合作培养)，博士后研究员就职于美国伊利诺伊大学 (University of Illinois at Urbana-Champaign)。主要从事社会统计及计量经济分析、人口健康学等领域的研究与教学。2013 年入选教育部新世纪优秀人才计划，兼任北京大学社会科学方法培训教授、美国 PAA、国际 IUSSP 会员及 IUSSP 社会科学定量方法培训专家组成员、美国伊利诺伊大学合作研究员及中国青少年性健康教育委员会副主任委员等。曾在《中国社会科学》，《社会学研究》，_Chinese Sociological Review_, _Schizophrenia Research_ 等期刊发表论文近百篇，出版著作 6 部；主持国家社科基金项目 2 项、横向课题 12 项。

**小插曲：** 王存同教授长期兼任北京大学等多个高校社会科学方法的培训教授，授课生动活泼，通俗易懂，拥有众多粉丝。每逢王老师在中央财经大学沙河校区 (距离市区 1.5 小时车程) 开讲时，小伙伴们便像候鸟般赶赴沙河。每次上课总是座无虚席，或席地而坐、或背墙而立……若想在教室前排的过道台阶上抢个位置，那可是要早早出动的。

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/司继春-工作照-半身.png)

[司继春](http://www.suibe.edu.cn/txxy/b1/2d/c5782a45357/page.htm)， 上海对外经贸大学统计与信息学院讲师，主要研究领域为微观计量经济学、产业组织理论。在 Journal of Business and Economic Statistics、《财经研究》等学术刊物上发表多篇论文。其实，大家更熟悉的是知乎上大名鼎鼎的 [**慧航**](https://www.zhihu.com/people/sijichun/columns)，拥有近 30 万个关注者，获得过 42w+ 次赞同，他就是司继春老师 —— [**知乎-慧航**](https://www.zhihu.com/people/sijichun/columns)。

&emsp;


## 3. 课程详情

&emsp;

> &#x1F353; [下载-课程中涉及的论文合集](https://www.jianguoyun.com/p/DUasG80QtKiFCBjHpskD)

&emsp;

因果推断 (causal inference)，缘起于人类的本能或经常自发提出的一个简单问题：为什么 (why)？事实上，作为科学研究的核心与宗旨，「**因果推断**」就是关于此问题的严肃思考、科学验证或因果分析 (causal analysis)。

目前，因果推断模型与方法受到了经济学、金融学、社会学、管理学、人口学及公共卫生等领域的重视和青睐，并成为实证研究的核心利器。但因果推断并非轻而易举之事，正如约翰・杜威 (John Dewey) 所述，「**科学的法则与规律，并不位于自然界的表层，而是隐藏在暗处，我们必须主动利用精心设计的探索技巧把它们从自然中剥离出来。**」

为此，我们邀请了量化分析及实证研究的著名大咖、中央财经大学的王存同教授、上海对外经贸大学讲师司继春与大家分享因果推断的基本概念、主要模型和 Stata 实现方法。

通过本期课程的学习，我们期望大家能从在如下几个方面有所收获：
- 因果推断理论基础和模型构建的核心思想是什么？
- 如何从 OLS 过渡到因果推断？
- 实证分析中有哪些进行因果推断的模型和工具？它们的前提条件和局限是什么？
- 如何界定研究中的「因果关系」？
- 如何与审稿人对话？

&emsp; 

### Part A：王存同 老师

> #### **第 1 讲** &ensp; 从多元回归走向因果推断 (3 小时)
- 作为因果推断的基础——多元回归
- 基本假定与计量模型的脉络及发展
- 计量建模原则与分析策略
- 案例与Stata操作
- 扩展阅读：
  - Levitt, S. D. (2004). Understanding why crime fell in the 1990s: Four factors that explain the decline and six that do not. Journal of Economic Perspectives, 18(1), 163-190. [-PDF-](https://www.jianguoyun.com/p/DaRnT3YQtKiFCBj2yb8D)
  - Angrist, J. D., & Pischke, J. S. (2008). Mostly Harmless Econometrics: An Empiricist's Companion. Princeton university press.
  - Morgan, S. L., & Winship, C. (2007). Counterfactuals and Causal Inference: Methods and Principles for Social Research. New York, NY. Cambridge University Press.

> #### **第 2 讲** &ensp; Rubin 因果模型及扩展 (3 小时)
- 回归模型与因果解释
- 尤尔-辛普森悖论
- Rubin因果模型 (Rubin Causal Model, RCM) 
- 决策理论因果模型 (decision-theoretic causality) 
- 因果图 (Causal Diagram: DAGs) 
- 扩展阅读：
  - Pearl, J. (2009). Causality: Models, Reasoning, and Inference (2nd). New York: Cambridge University Press.
  - Pearl, J., & Mackenzie, D. (2018). The Book of Why: the New Science of Cause and Effect. CITIC press.
  - Morgan, S. L., & Winship, C. (2007). Counterfactuals and Causal Inference: Methods and Principles for Social Research. New York, NY. Cambridge University Press.
  - Imbens, G. W., & Rubin, D. B. (2015). Causal Inference in Statistics, Social, and Biomedical sciences. Cambridge University Press.

> #### **第 3 讲** &ensp; 内生性问题与工具变量法 (IV) (3 小时)
- 内生性问题与解决路径
- 工具变量选择的方法及分类
- 工具变量的检验
- 工具变量回归模型 (2SLS+GMM) 
- 案例与Stata操作
- 扩展阅读：
  - Qian, N. (2008). Missing women and the price of tea in China: The effect of sex-specific earnings on sex imbalance. The Quarterly Journal of Economics, 123(3), 1251-1285. [-PDF-](https://www.jianguoyun.com/p/DcX5s8EQtKiFCBj4yb8D)
  - Waldman, M., Nicholson, S., & Adilov, N. (2006). Does Television Cause Autism? (No. w12632). National Bureau of Economic Research. Working Paper. [-PDF-](https://www.jianguoyun.com/p/DZeiWdEQtKiFCBj9yb8D)
  - Hoxby, C. M. (2000). Does competition among public schools benefit students and taxpayers?. American Economic Review, 90(5), 1209-1238. [-PDF-](https://www.jianguoyun.com/p/Dc8o2-cQtKiFCBiByr8D)

> #### **第 4 讲** &ensp; 断点回归 (RDD) (3 小时)
- 断点回归原理
- 精确断点回归
- 模糊断点回归
- 断点回归作用及适用条件- 案例与Stata操作
- 扩展阅读：
  - Almond, D., Chen, Y., Greenstone, M., & Li, H. (2009). Winter heating or clean air? Unintended impacts of China's Huai river policy. American Economic Review, 99(2), 184-90. [-PDF-](https://www.jianguoyun.com/p/DXg6YlIQtKiFCBiFyr8D)
  - 雷晓燕, 谭力, & 赵耀辉. (2010). 退休会影响健康吗?. 经济学 (季刊), 9(4), 1539-1558. [-PDF-](https://www.jianguoyun.com/p/DfJb7jkQtKiFCBiKyr8D)


&emsp;

### Part B：司继春 老师

> #### **第 5 讲** &ensp; 线性面板数据基础 (3 小时)

- 固定效应、随机效应及其选择
- 一阶差分、LSDV 等估计量介绍
- 面板数据各估计量之间的关系
- 线性面板数据模型的 Stata 实现
- 交互固定效应
- 应用：Gentzkow et al. (2011)
  - Gentzkow M, Shapiro J M, Sinkinson M. The effect of newspaper entry and exit on electoral politics. American Economic Review, 2011, 101(7): 2980-3018. [-PDF-](https://www.jianguoyun.com/p/DRGv-6YQtKiFCBjWj8AD), [-PPT-](https://www.jianguoyun.com/p/Ddwzcp8QtKiFCBjZj8AD), [-Data Link-](http://www.icpsr.umich.edu/icpsrweb/ICPSR/studies/30261/version/6), [-Cited-](https://academic.microsoft.com/paper/2031266171/reference/search?q=The%20Effect%20of%20Newspaper%20Entry%20and%20Exit%20on%20Electoral%20Politics&qe=%2540%2540%2540REFERENCES%253D2031266171&f=&orderBy=0)

> #### **第 6 讲** &ensp; 双重差分模型及其拓展 (3 小时)
- 双重差分模型的基本设定
- 共同趋势假设的检验
- 处理效应的时间异质性
- 应用及 Stata 实现：Wolfers (2006) 相关论文
  - Stevenson, B., J. Wolfers, 2006, Bargaining in the shadow of the law: Divorce laws and family distress, The Quarterly Journal of Economics, 121 (1): 267-288. [-PDF-](https://www.jianguoyun.com/p/Dd4p7r0QtKiFCBiuyr8D), [-Link-](https://academic.microsoft.com/paper/2162332627/citedby/search?q=Bargaining%20in%20the%20Shadow%20of%20the%20Law%3A%20Divorce%20Laws%20and%20Family%20Distress&qe=RId%253D2162332627&f=&orderBy=0)
- 断点双重差分、模糊双重差分
- 回归控制法 (HCW 方法)
  - Hsiao, C., H. S. Ching, S. K. Wan, 2012, A panel data approach for program evaluation: Measuring the benefits of political and economic integration of hong kong with mainland china, JOURNAL OF APPLIED ECONOMETRICS, 27 (5): 705-740. [-PDF-](https://www.jianguoyun.com/p/DaTvgGkQtKiFCBipyr8D), [-Link-](https://academic.microsoft.com/paper/1995837384/citedby/search?q=A%20Panel%20Data%20Approach%20for%20Program%20Evaluation%20%E2%80%94%20Measuring%20the%20Benefits%20of%20Political%20and%20Economic%20Integration%20of%20Hong%20Kong%20with%20Mainland%20China&qe=RId%253D1995837384&f=&orderBy=0) 

> #### **第 7 讲** &ensp; 无混淆分配下的因果推断：匹配及拓展 (3 小时)
- 无混淆分配假设
- 无混淆分配假设下的推断：匹配
- 匹配的 Stata 实现：Imbens([2015](https://www.jianguoyun.com/p/De0KzkYQtKiFCBidyr8D)) 中的例子
- 倾向得分匹配的原理及实现
- 逆概率加权法、双向稳健的逆概率加权方法
- 扩展阅读：
  - Imbens, G. W., D. B. Rubin. Causal inference for statistics, social, and biomedical sciences: An introduction[M]. Cambridge University Press, 2015. [-Link-](https://www.jianguoyun.com/p/De0KzkYQtKiFCBidyr8D)

> #### **第 8 讲** &ensp; 样本选择模型与自选择模型 (3 小时)
- 删失数据分析介绍
- 样本选择模型介绍
- 自选择模型
- 模型的区别、联系及应用
- Stata 实操


&emsp;

## 4. 报名信息
- **主办方：** 太原君泉教育咨询有限公司
- **标准费用**：3900 元/人 (全价)
- **优惠方案**：
  - **连享会老学员：** 3600 元/人
  - **团购 (3 人以上)：** 3500 元/人
  - **温馨提示：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 王老师：18903405450 (微信同号)
  - 李老师：‭18636102467 (微信同号)

> **特别提示：**
>
> 为保护讲师的知识产权和您的账户安全，系统会自动在您观看的视频中嵌入您的「**用户名、ID 和 IP 地址**」信息，每个 ID 只能锁定一台设备进行观看，以切实保护您的权益。

&emsp;

> **报名链接：** [http://junquan18903405450.mikecrm.com/HFMDelY](http://junquan18903405450.mikecrm.com/HFMDelY)

> &#x23E9; 长按/扫描二维码报名：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/因果推断报名二维码180.png)

### 缴费方式

> **方式 1：对公转账**

- 户名：太原君泉教育咨询有限公司
- 账号：35117530000023891 (山西省太原市晋商银行南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式 2：扫码支付**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会2020暑期支付二维码180.png)

> **温馨提示：** 扫码支付后，请将「**付款记录**」截屏发给王老师-18903405450（微信同号）

&emsp;

&emsp;

<div STYLE="page-break-after: always;"></div>


> **课程主页：** <https://gitee.com/arlionn/YG>

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;

&emsp;

---
>### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。




&emsp;

### 相关课程


> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  


&emsp;

---
### 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)** &#x2B50;|  | DSGE, 因果推断, 空间计量等  |
| &#x2B55; **[Stata数据清洗](https://lianxh.duanshu.com/#/brief/course/c5193f0e6e414a7e889a8ff9aeb4aaef)** | 游万海| [直播, 2 小时](https://www.lianxh.cn/news/f785de82434c1.html)，已上线 |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
| 面板模型 | 连玉君  | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |


> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)


&emsp;





